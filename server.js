// Importing node modules
import dotenv from 'dotenv';

// Importing source files
import app from './lib/express/index';

app.start();

