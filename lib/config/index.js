const port = process.env.PORT || 3000;
const env = process.env.NODE_ENV || '';

export default {
    port: port,
    live : env === 'production',
    development : env === 'development'
}