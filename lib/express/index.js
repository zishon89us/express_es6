// Importing node modules
import express from 'express';
import dotenv from 'dotenv';
import bodyParser from 'body-parser';
import busboy from 'connect-busboy';
import compression from 'compression';
import Debug from 'debug';

if (process.env.NODE_ENV === "development") {
    dotenv.config({silent: true});
}

import config from '../config/index';
import routes from '../../src/routes/routes';

const debug = Debug('main');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(busboy());
app.use(compression());

app.use('/', routes);

process.on('uncaughtException', function(err) {
    debug('%s', err);
});

export default {
    app : app,
    start:function(){

       const server = app.listen(config.port, () => {
        //const {address, port} = server.address();
        //console.log(`Example app listening at http:${address}:${port}`);
    });
    }
};
