# Express ES6 Hello world

### Install dependencies

> npm install

### Install nodemon globally

> npm install -g nodemon

### start the server

> npm start

### open in browser
http://localhost:3000 (or the port configured with)

### build for production (output to dist folder)
npm run build